import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

from myenum import myconstants


def start_analyze():
    data_frame = read_final_csv()
    #main_hist(data_frame)
    main_heat_map(data_frame)
    main_temperature(data_frame)
    main_gyro(data_frame)
    main_magnetometer(data_frame)


def read_final_csv():
    analysis_dir = myconstants.analysis_dir
    file_name = myconstants.preprocessed_file_name
    return pd.read_csv(analysis_dir + '/' + file_name + myconstants.csv_file_type)


def main_hist(data_frame):
    ax = data_frame[myconstants.useful_column_arr].hist()
    fig = ax.get_figure()
    analysis_dir = myconstants.analysis_dir
    file_name = myconstants.main_hist_file_name
    ax.figure.savefig(analysis_dir + '/' + file_name + myconstants.png_file_type)


def main_heat_map(data_frame):
    method = myconstants.pearson_method
    corr = data_frame.corr(method=method)
    plt.figure(figsize=(15, 8))
    hm = sns.heatmap(corr, annot=True)
    fig_heatmap = hm.get_figure()
    analysis_dir = myconstants.analysis_dir
    file_name = myconstants.main_heat_map_file_name + '_' + method
    fig_heatmap.savefig(analysis_dir + "/" + file_name + myconstants.png_file_type)


def main_temperature(data_frame):
    fig = data_frame[["CCSDS_SECONDS_SUBSECS", "TEMP_1", "TEMP_0"]].plot(x="CCSDS_SECONDS_SUBSECS").get_figure()
    analysis_dir = myconstants.analysis_dir
    file_name = myconstants.main_temperature_file_name
    fig.savefig(analysis_dir + "/" + file_name + myconstants.png_file_type)

    fig = data_frame[["CCSDS_SECONDS_SUBSECS", "TEMP_1"]].plot(x="CCSDS_SECONDS_SUBSECS").get_figure()
    analysis_dir = myconstants.analysis_dir
    file_name = "TEMP_1"
    fig.savefig(analysis_dir + "/" + file_name + myconstants.png_file_type)

    fig = data_frame[["CCSDS_SECONDS_SUBSECS", "TEMP_0"]].plot(x="CCSDS_SECONDS_SUBSECS").get_figure()
    analysis_dir = myconstants.analysis_dir
    file_name = "TEMP_0"
    fig.savefig(analysis_dir + "/" + file_name + myconstants.png_file_type)


def main_gyro(data_frame):
    fig = data_frame[["CCSDS_SECONDS_SUBSECS", "GYRO_0", "GYRO_1", "GYRO_2"]].plot(x="CCSDS_SECONDS_SUBSECS").get_figure()
    analysis_dir = myconstants.analysis_dir
    file_name = myconstants.main_gyro_file_name
    fig.savefig(analysis_dir + "/" + file_name + myconstants.png_file_type)

    fig = data_frame[["CCSDS_SECONDS_SUBSECS", "GYRO_0"]].plot(x="CCSDS_SECONDS_SUBSECS").get_figure()
    analysis_dir = myconstants.analysis_dir
    file_name = "GYRO_0"
    fig.savefig(analysis_dir + "/" + file_name + myconstants.png_file_type)

    fig = data_frame[["CCSDS_SECONDS_SUBSECS", "GYRO_1"]].plot(x="CCSDS_SECONDS_SUBSECS").get_figure()
    analysis_dir = myconstants.analysis_dir
    file_name = "GYRO_1"
    fig.savefig(analysis_dir + "/" + file_name + myconstants.png_file_type)

    fig = data_frame[["CCSDS_SECONDS_SUBSECS", "GYRO_2"]].plot(x="CCSDS_SECONDS_SUBSECS").get_figure()
    analysis_dir = myconstants.analysis_dir
    file_name = "GYRO_2"
    fig.savefig(analysis_dir + "/" + file_name + myconstants.png_file_type)

    fig = data_frame[["CCSDS_SECONDS_SUBSECS", "GYRO_TEMP"]].plot(x="CCSDS_SECONDS_SUBSECS").get_figure()
    analysis_dir = myconstants.analysis_dir
    file_name = "GYRO_TEMP"
    fig.savefig(analysis_dir + "/" + file_name + myconstants.png_file_type)


def main_magnetometer(data_frame):
    fig = data_frame[["CCSDS_SECONDS_SUBSECS", "MAG_0", "MAG_1", "MAG_2"]].plot(x="CCSDS_SECONDS_SUBSECS").get_figure()
    analysis_dir = myconstants.analysis_dir
    file_name = myconstants.main_magnetometer_file_name
    fig.savefig(analysis_dir + "/" + file_name + myconstants.png_file_type)

    fig = data_frame[["CCSDS_SECONDS_SUBSECS", "MAG_0"]].plot(x="CCSDS_SECONDS_SUBSECS").get_figure()
    analysis_dir = myconstants.analysis_dir
    file_name = "MAG_0"
    fig.savefig(analysis_dir + "/" + file_name + myconstants.png_file_type)

    fig = data_frame[["CCSDS_SECONDS_SUBSECS", "MAG_1"]].plot(x="CCSDS_SECONDS_SUBSECS").get_figure()
    analysis_dir = myconstants.analysis_dir
    file_name = "MAG_1"
    fig.savefig(analysis_dir + "/" + file_name + myconstants.png_file_type)

    fig = data_frame[["CCSDS_SECONDS_SUBSECS", "MAG_2"]].plot(x="CCSDS_SECONDS_SUBSECS").get_figure()
    analysis_dir = myconstants.analysis_dir
    file_name = "MAG_2"
    fig.savefig(analysis_dir + "/" + file_name + myconstants.png_file_type)


def save_fig_common(fig, file_name):
    analysis_dir = myconstants.analysis_dir
    fig.savefig(analysis_dir + "/" + file_name + myconstants.png_file_type)

#start_analyze()
