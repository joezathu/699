import os
import pandas as pd
from myenum import myconstants


def create_dir():
    analysis_dir = myconstants.analysis_dir
    if not os.path.exists(analysis_dir):
        os.mkdir(analysis_dir)
    return analysis_dir


def output_csv(data_frame, file_name):
    output_dir = create_dir()
    data_frame.to_csv(output_dir + '/' + file_name + myconstants.csv_file_type, index=False)


def output_png(fig, file_name):
    output_dir = create_dir()
    fig.savefig(output_dir + '/' + file_name + myconstants.png_file_type)
