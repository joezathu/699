import datetime
import os
import glob
import pandas as pd
from myenum import myconstants
import zipfile

ui_date_format = myconstants.ui_date_format
file_date_format = myconstants.file_date_format


def start_read(from_date, to_date):
    validate_result = validate_date(from_date, to_date)
    if validate_result:
        return False
    else:
        from_date = datetime.datetime.strptime(from_date, ui_date_format)
        to_date = datetime.datetime.strptime(to_date, ui_date_format)
    day_range = (to_date - from_date).days
    day_array = []
    day = 0
    while day < day_range:
        day_array.append((from_date + datetime.timedelta(days=day)).strftime(file_date_format))
        day += 1
    print(day_array)
    in_dir = myconstants.original_dir
    out_dir = myconstants.preprocess_in_dir
    required_file = myconstants.support_file_type
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)

    is_zip = zipfile.is_zipfile(in_dir + '/data.zip')
    if is_zip:  # zipfile exists, go with zipfile
        z = zipfile.ZipFile(in_dir + '/data.zip', 'r')
        file_list = z.namelist()
        for i in range(0, day_range):
            for current_file in file_list:
                if required_file in current_file and day_array[i] in current_file:
                    print(current_file)
                    current_file_name = os.path.basename(current_file)
                    file = z.open(current_file)
                    frame = pd.read_csv(file)
                    frame = frame.iloc[:, :-1]
                    frame['filename'] = current_file_name
                    frame.to_csv(out_dir + "/" + current_file_name, index=False)
        z.close()
    else:
        for i in range(0, day_range):
            filepath = in_dir + "/" + day_array[i] + "/*" + required_file + ".csv"
            temp_globed_files = glob.glob(filepath)
            for csv in temp_globed_files:
                current_file_name = os.path.basename(csv)
                if required_file in current_file_name:
                    print(current_file_name)
                    frame = pd.read_csv(csv)
                    frame = frame.iloc[:, :-1]
                    frame['filename'] = current_file_name
                    frame.to_csv(out_dir + "/" + current_file_name, index=False)


def validate_period(from_date, to_date):
    today = datetime.datetime.strptime(datetime.datetime.today().__str__(), ui_date_format)
    if from_date > today or to_date > today:
        return False
    else:
        return True


def validate_gap(from_date, to_date):
    date_diff = to_date - from_date
    if date_diff < 0:
        return False
    else:
        return True


def validate_date(from_date, to_date):
    try:
        from_date = datetime.datetime.strptime(from_date, ui_date_format)
        to_date = datetime.datetime.strptime(to_date, ui_date_format)
        if validate_period(from_date, to_date) and validate_gap(from_date, to_date):
            return True
        else:
            return False
    except ValueError:
        return False
