from enum import Enum, unique

@unique
class ColumnNames(Enum):
    temp_0 = "TEMP_0"
    temp_1 = "TEMP_1"
    mag_0 = "MAG_0"
    mag_1 = "MAG_1"
    mag_2 = "MAG_2"
    gyro_0 = "GYRO_0"
    gyro_1 = "GYRO_1"
    gyro_2 = "GYRO_2"
    gyro_t = "GYRO_TEMP"
