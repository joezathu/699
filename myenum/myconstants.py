import os


current_path = os.getcwd()
support_file_type = 'SEN_Hk_tlm_t'
original_dir = current_path+"/OperationalData"
preprocess_in_dir = current_path + '/result/data'
ui_date_format = '%Y_%m_%d'
file_date_format = '%Y_%m_%d'
preprocessed_file_name = 'Preprocessed_SEN_Hk_tlm_t'
iqr_threshold = 3
csv_file_type = '.csv'
png_file_type = '.png'
csv_separator = ','
analysis_dir = current_path + '/result/analysis'
useful_column_arr = ["TEMP_0", "TEMP_1", "MAG_0", "MAG_1", "MAG_2", "GYRO_0", "GYRO_1", "GYRO_2", "GYRO_TEMP"]
main_hist_file_name = 'Histogram_of_Sensors'
main_heat_map_file_name = 'Heatmap'
main_temperature_file_name = 'Temperature'
main_gyro_file_name = 'Gyro'
main_magnetometer_file_name = 'Magnetometer'
pearson_method = 'pearson'
time_conversion_slope = 0.9998536124
time_conversion_intercept = -1278895302.83
