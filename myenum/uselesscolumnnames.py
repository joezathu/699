from enum import Enum, unique


@unique
class UselessColumnNames(Enum):
    target = "TARGET"
    packet = "PACKET"
    scid = "STF1_SCID"
    fifo = "STF1_FIFO"
    pketver = "CCSDS_PKT_VER"
    pkttyp = "CCSDS_PKT_TYP"
    secflg = "CCSDS_SEC_FLG"
    apid = "CCSDS_APID"
    seqflags = "CCSDS_SEQ_FLAGS"
    lengths = "CCSDS_LENGTH"
    cmdcount = "CMD_COUNT"
    errcount = "CMD_ERROR_COUNT"
    timeformatted = "RECEIVED_TIMEFORMATTED"
    timeseconds = "RECEIVED_TIMESECONDS"
    rcvcount = "RECEIVED_COUNT"
