from myenum import myconstants
import pandas as pd
import numpy as np
from sklearn.linear_model import LinearRegression
import matplotlib.pyplot as plt
import random


def start_predict():
    data_frame = read_final_csv()
    predict_temperature(data_frame)


def predict_temperature(data_frame):
    fig = data_frame[["CCSDS_SECONDS_SUBSECS", "TEMP_1", "TEMP_0"]].plot(x="CCSDS_SECONDS_SUBSECS").get_figure()
    analysis_dir = myconstants.analysis_dir
    file_name = "Predict_Temperature"
    fig.savefig(analysis_dir + "/" + file_name + myconstants.png_file_type)

    fig = data_frame[["CCSDS_SECONDS_SUBSECS", "TEMP_1"]].plot(x="CCSDS_SECONDS_SUBSECS").get_figure()
    analysis_dir = myconstants.analysis_dir
    file_name = "Predict_TEMP0"
    fig.savefig(analysis_dir + "/" + file_name + myconstants.png_file_type)

    fig = data_frame[["CCSDS_SECONDS_SUBSECS", "TEMP_0"]].plot(x="CCSDS_SECONDS_SUBSECS").get_figure()
    analysis_dir = myconstants.analysis_dir
    file_name = "Predict_TEMP1"
    fig.savefig(analysis_dir + "/" + file_name + myconstants.png_file_type)


def read_final_csv():
    analysis_dir = myconstants.analysis_dir
    file_name = "Final_SEN_Hk_tlm_t"
    return pd.read_csv(analysis_dir + '/' + file_name + myconstants.csv_file_type)


def load_data_set(file_name):
    num_feat = len(open(file_name).readline().split('\t')) - 1
    data_mat = []
    label_mat = []
    fr = open(file_name)
    for line in fr.readlines():
        line_arr = []
        cur_line = line.strip().split('\t')
        for i in range(num_feat):
            line_arr.append(float(cur_line[i]))
        data_mat.append(line_arr)
        label_mat.append(float(cur_line[-1]))
    return data_mat, label_mat


def stand_regress(x_arr, y_arr):
    x_mat = np.mat(x_arr)
    y_mat = np.mat(y_arr).T
    x_tx = x_mat.T * x_mat
    if np.linalg.det(x_tx) == 0.0:
        print("This matrix is singular, cannot do inverse")
        return
    ws = x_tx.I * (x_mat.T * y_mat)
    return ws


def skl_line_reg():
    xs = range(100)
    ys = []
    for x in xs:
        ys.append(5*x+2+random.random()*50)
    model = LinearRegression()
    model.fit([[x] for x in xs], ys)
    ys_ = model.predict([[x] for x in xs])
    plt.scatter(xs, ys, marker='.')
    plt.scatter(xs, ys_, marker='+')
    plt.show()



