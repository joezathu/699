import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import glob
import os

from myenum import myconstants
from myenum.columnnames import ColumnNames

from myenum.filetype import FileType
from fileio import outputfile
from myenum.uselesscolumnnames import UselessColumnNames


def start_preprocess():
    folder_name = myconstants.preprocess_in_dir
    file_type = myconstants.csv_file_type
    separator = myconstants.csv_separator
    concat_dataframe = pd.concat([pd.read_csv(f, sep=separator) for f in glob.glob(folder_name + "/*" + file_type)],
                                 ignore_index=True)
    df_no_na = remove_useless_columns(concat_dataframe, 'SEN_HK_tlm_t')
    df_no_duplicate = remove_duplicates(df_no_na)
    df_no_outlier = df_no_duplicate
    for column_name in ColumnNames:
        df_no_outlier = define_max_min_mad_iqr(df_no_outlier, column_name.value, myconstants.iqr_threshold)
    df_no_outlier = df_no_outlier.sort_values(['filename', 'CCSDS_SEQ_COUNT'], ascending=[True, True])
    outputfile.output_csv(df_no_outlier, myconstants.preprocessed_file_name)


def remove_duplicates(input_dataframe):
    input_dataframe["CCSDS_SECONDS_SUBSECS"] = input_dataframe["CCSDS_SECONDS"].astype(str) + '-' + input_dataframe["CCSDS_SUBSECS"].astype(str)
    return input_dataframe.drop_duplicates(subset='CCSDS_SECONDS_SUBSECS', keep="first")


def remove_useless_columns(input_dataframe, file_type):
    if FileType(file_type) == FileType('SEN_HK_tlm_t'):
        for column_name in UselessColumnNames:
            input_dataframe.pop(column_name.value)

        return input_dataframe.dropna()


def remove_outliers(df, column_string, set_minimum, set_maximum):
    dataframe = df[df[column_string] > set_minimum]
    dataframe = dataframe[dataframe[column_string] < set_maximum]
    return dataframe


def define_max_min_delta_nd(df, column_string):
    mean = df[column_string].mean()
    standard = df[column_string].std()
    minvalue = mean - 3 * standard
    maxvalue = mean + 3 * standard
    return minvalue, maxvalue


def define_max_min_mad_iqr(df, column_string, threshold):
    clm = df[column_string]
    median = np.median(clm)
    absolute = np.absolute(clm-median)
    abs_me = np.median(absolute)
    minvalue = median - threshold * abs_me
    maxvalue = median + threshold * abs_me
    return remove_outliers(df, column_string, minvalue, maxvalue)


def get_cdsec(seconds, sub_seconds):
    return seconds + sub_seconds/pow(2, 32)


def time_conversion(seconds, sub_seconds):
    cdsec = get_cdsec(seconds, sub_seconds)


#start_preprocess()

