from flask import Flask, render_template, request, redirect
from flask_wtf import Form
from wtforms import DateField
import os
from preprocess import preprocess as prp
from fileio import readfile as rd
from analysis import analyze as ana
from prediction import predict as prd

cwd = os.getcwd()
app = Flask(__name__, static_folder=cwd + "/result")
app.secret_key = 'SHH!'


class DateForm(Form):
    dt = DateField('Pick a Date', format="%Y_%m_%d")


@app.route("/")
def home():
    current_path = os.path.abspath('.')
    entry_html_path = 'main.html'
    form = DateForm()
    if form.validate_on_submit():
        return form.dt.data.strftime('%x')
    return render_template(entry_html_path, form=form)


@app.route("/readfiles", methods=['POST'])
def readfile():
    from_date = request.form['fromDate']
    to_date = request.form['toDate']
    rd.start_read(from_date, to_date)
    return redirect("preprocess")


@app.route("/preprocess")
def preprocess():
    prp.start_preprocess()
    return redirect("analyze")


@app.route("/analyze")
def analyze():
    ana.start_analyze()
    return redirect("predict")


@app.route("/predict")
def predict():
    prd.start_predict()
    return redirect("result")


@app.route("/result")
def result():
    print(os.getcwd())
    result_html_path = 'result.html'
    return render_template(result_html_path)


if __name__ == "__main__":
    app.run(host='0.0.0.0')
    #app.run(host='127.0.0.1', debug=True)
